import moment from "moment";
// import { extendMoment } from 'moment-range'
// import'moment-range'
import { extendMoment } from "moment-range";

export default class Month {
  constructor(month, year) {
    this.momentRange = extendMoment(moment);
    this.start = moment({ month, year });
    this.end = this.start.clone().endOf("month");
    this.month = month;
    this.year = year;
  }

  getWeekStart() {
    return this.start.weekday();
  }

  getDays() {
    const dateRange = this.momentRange.range(this.start, this.end).by("days");
    console.log("#", dateRange);
    // console.log(Array.from(dateRange.by))
    return dateRange;
  }

  getFormattedMonth() {
    return this.start.format("MMMM YYYY");
  }
}
